﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using exercicio_funcionario.Repositorio;
using exercicio_funcionario.Entidade;

namespace exercicio_funcionario.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class FuncionarioController : ControllerBase
    {
        
        IConfiguration _configuration;
        RepositorioFuncionario repFuncionario;
        public FuncionarioController(IConfiguration configuration)
        {
            this._configuration = configuration;
            this.repFuncionario = new RepositorioFuncionario(configuration);
        }
        [HttpGet]
        public ActionResult<IEnumerable<dynamic>> GetTodosFuncionarios()
        {
            return new ObjectResult(repFuncionario.ObterTodos());
        }

        [HttpGet("{id}")]
        public ActionResult<dynamic> GetFuncionario(int id)
        {
            if(repFuncionario.ObterTodos().Any(x => x.ID == id))
            {
                return new ObjectResult(repFuncionario.ObterFuncionario(id));
            }
            return new NotFoundResult();
        }

        [HttpPost]
        public ActionResult InserirFuncionario([FromBody] Funcionario func)
        {
            
            if(func.Nome == null || func.Salario == 0 || func.Cargo == null)
            {
                return new BadRequestResult();
            }
            repFuncionario.InserirFuncionario(func);
            return new CreatedResult("Criado com sucesso...",func);
        }

        [HttpPut("{id}")]
        public ActionResult AtualizarFuncionario(int id, [FromBody] Funcionario func)
        {
            if(func.Nome == null || func.Salario == 0 || func.Cargo == null)
            {
                return new BadRequestResult();
            }
            if(repFuncionario.ObterTodos().Any(x => x.ID == id) == false)
            {
                return new NotFoundResult();
            }
            repFuncionario.AtualizarFuncionario(id,func);
            return new OkObjectResult("Atualizado com sucesso...");
        }

        [HttpDelete("{id}")]
        public ActionResult DeletarFuncionario(int id)
        {
             if(repFuncionario.ObterTodos().Any(x => x.ID == id))
            {
                repFuncionario.DeletarFuncionario(id);
                return new OkObjectResult("Deletado com sucesso...");
            }
            return new NotFoundResult();
        }
    }
}
