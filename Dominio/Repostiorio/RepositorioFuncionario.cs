using Dapper;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using exercicio_funcionario.Entidade;

namespace exercicio_funcionario.Repositorio
{
    public class RepositorioFuncionario
    {
        IConfiguration _configuration;
        Connection _connection;
        public RepositorioFuncionario(IConfiguration configuration)
        {
            this._configuration = configuration;
            _connection = new Connection(configuration);
        }
        public IEnumerable<dynamic> ObterTodos()
        {
            var query = @"SELECT ID,Nome,CPF,Idade,Cargo,Salario,Data_Admissao 
                          FROM _funcionarios;";
            using(var con = _connection.Conect())
            {
                return con.Query<dynamic>(query).Select(x => new Funcionario(x.ID,
                                                                             x.Nome,
                                                                             x.CPF,
                                                                             x.Idade,
                                                                             x.Cargo,
                                                                             x.Salario,
                                                                             x.Data_Admissao));
            }
        }
        public Funcionario ObterFuncionario(int id)
        {
            var query = @"SELECT ID,Nome,CPF,Idade,Cargo,Salario,Data_Admissao 
                          FROM _funcionarios
                          WHERE ID = @ID;";
            using(var con = _connection.Conect())
            {
                return con.Query<dynamic>(query,new{id}).Select(x => new Funcionario(x.ID,
                                                                                     x.Nome,
                                                                                     x.CPF,
                                                                                     x.Idade,
                                                                                     x.Cargo,
                                                                                     x.Salario,
                                                                                     x.Data_Admissao)).FirstOrDefault();
            }              
        }
        public void InserirFuncionario(Funcionario funcionario)
        {
            var param = new DynamicParameters();
            param.Add("@Nome",funcionario.Nome);
            param.Add("@CPF",funcionario.CPF);
            param.Add("@Idade",funcionario.Idade);
            param.Add("@Cargo",funcionario.Cargo);
            param.Add("@Salario",funcionario.Salario);
            param.Add("@Data_Admissao",funcionario.Data_Admissao);
            string query = @"INSERT INTO _funcionarios
                             VALUES(@Nome,@CPF,@Idade,@Cargo,@Salario,@Data_Admissao);";
            using (var con = _connection.Conect())
            {
                con.Query(query,param);
            }
        }
        public void AtualizarFuncionario(int id,Funcionario funcionario)
        {
            var param = new DynamicParameters();
            param.Add("@ID",id);
            param.Add("@Nome",funcionario.Nome);
            param.Add("@CPF",funcionario.CPF);
            param.Add("@Idade",funcionario.Idade);
            param.Add("@Cargo",funcionario.Cargo);
            param.Add("@Salario",funcionario.Salario);
            param.Add("@Data_Admissao",funcionario.Data_Admissao);
            var query = @"UPDATE _funcionarios
                          SET Nome = @Nome,
                              CPF = @CPF,
                              Idade = @Idade,
                              Cargo = @Cargo,
                              Salario = @Salario,
                              Data_Admissao = @Data_Admissao
                              WHERE ID = @ID;";
            using (var con = _connection.Conect())
            {
                con.Query(query,param);
            }
        }

        public void DeletarFuncionario(int id)
        {
            var query = @"DELETE FROM _funcionarios WHERE ID = @ID;";
            using(var con = _connection.Conect())
            {
                con.Query(query,new{id});
            }
        }

    }
}