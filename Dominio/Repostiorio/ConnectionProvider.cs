using System.Data.SqlClient;
using System.Data;
using Microsoft.Extensions.Configuration;

namespace exercicio_funcionario.Repositorio
{
    public class Connection
    {
        private IConfiguration _configuration;
        public Connection(IConfiguration configuration)
        {
            this._configuration = configuration;
        }
        public IDbConnection Conect()
        {
            string strCon = _configuration.GetConnectionString("FuncionarioConnection");
            return new SqlConnection(strCon);
        }
        
    }
}