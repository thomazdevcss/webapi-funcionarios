namespace exercicio_funcionario.Entidade
{
    public class Funcionario
    {
        public Funcionario(int id,string nome,string cpf,int idade,string cargo,double salario,string data_Admissao)
        {
            this.ID = id;
            this.Nome = nome;
            this.CPF = cpf;
            this.Idade = idade;
            this.Cargo = cargo;
            this.Salario = salario;
            this.Data_Admissao = data_Admissao;
        }
        public int ID { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public int Idade { get; set; }
        public string Cargo { get; set; }
        public double Salario { get; set; }
        public string Data_Admissao { get; set; }
    }
}